import java.util.ArrayList;
import java.util.HashMap;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        //[SECTION] ARRAY
        // Declare an array to store the first 5 prime numbers
        int[] primeNumbers = new int[5];

        // Assign the first 5 prime numbers to their respective indices
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        // Output specific elements of the array by specifying their indices
        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);

    //[SECTION]ARRAYLIST

        // Create an ArrayList of String data-type elements

        ArrayList<String> friends = new ArrayList<>();

        // Add 4 elements to the ArrayList
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        // Output the contents of the ArrayList with a message
        System.out.println("My friends are: " + friends);


//[SECTION]HASHMAPS
        // Create a HashMap with keys of type String and values of type Integer
        HashMap<String, Integer> inventory = new HashMap<>();

        // Add key-value pairs to the HashMap
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);


        // Output the contents of the HashMap with a message
        System.out.println("Our Current Inventory consists of:" + inventory);

    }
}